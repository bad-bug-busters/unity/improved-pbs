# Description
This repository features an improved version of HDRP's Physically Based Sky.
- Releated forum thread: https://forum.unity.com/threads/physically-based-sky.765962/
- Discord: https://discord.gg/gpSkrFGe8h

# Features
- Air scattering now accounts for an ozone layer.
https://forum.unity.com/threads/physically-based-sky.765962/page-3#post-7626310
- Enables spherical PBR fog.
https://forum.unity.com/threads/physically-based-sky.765962/page-3#post-7691800
- Avoids artifacts at low sun angles.
https://forum.unity.com/threads/physically-based-sky.765962/page-3#post-7733619
- Adds normal mapping and reflection support for the planet renderer.
https://forum.unity.com/threads/physically-based-sky.765962/page-3#post-7739907
- Adds parallax mapping support for the planet renderer.
https://forum.unity.com/threads/physically-based-sky.765962/page-3#post-7748796

# Extra Info
Files are based on Unity 2022.2.12f1 and HDRP 14.0.6, but all changes are marked with "//mod", so it's easy to merge to other versions.

It is recomended to re-install the official HDRP package each time Unity itself gets updated and then modify HDRP with this version again.
This step is required since Unity updates packages with each Unity update, without changing the packages version number.